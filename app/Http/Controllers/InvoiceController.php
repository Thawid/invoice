<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Representative;
use App\Models\Sell;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Medicine;
use App\Models\Customer;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\In;
use PDF;

class InvoiceController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Make Invoice Page
     *
     * */

    public function index()
    {
        $customer_list = $this->customer_list();
        $representative_list = $this->representative_list();
        return view('invoice.index')->with(compact('customer_list','representative_list'));
    }

    /*
     * Invoice list table
     *
     * */

    public function invoice_list()
    {
        return view('invoice.invoice-list');
    }

    /*
     * Get invoice list data
     *
     * */

    public function get_invoice_list(Request $request)
    {
        if ($request->ajax()) {

            DB::statement(DB::raw('set @rownum=0'));
            $data = DB::table('customers')
                ->join('sells', 'sells.customer_id', '=', 'customers.id')
                ->join('representatives','representatives.id','=','sells.representative_id')
                ->select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'customers.id','representatives.name as rp_name', 'customers.customer_name', 'sells.id as sell_id', 'sells.invoice_no', 'sells.total_qty', 'sells.total_bonus', 'sells.total_product_price', 'sells.created_at');
            //dd($data);
            return DataTables::of($data)
                ->addColumn('total_qty', function ($row) {
                    return en2bnNumber($row->total_qty);
                })
                ->addColumn('created_at', function ($row) {
                    $create_date = date_create($row->created_at);
                    $show_date = date_format($create_date,"d-m-Y");
                    return $show_date;
                })
                ->addColumn('total_bonus', function ($row) {
                    return en2bnNumber($row->total_bonus);
                })
                ->addColumn('total_product_price', function ($row) {
                    return en2bnNumber($row->total_product_price);
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="' . action('App\Http\Controllers\InvoiceController@invoice_view', ['id' => $row->sell_id]) . '" type="button" class="btn bg-success btn-sm">View</a>
                                            <a href="' . action('App\Http\Controllers\InvoiceController@invoice_update_view', ['id' => $row->sell_id]) . '" type="button" class="btn bg-warning btn-sm">Edit</a>

                                        </div>';
                    return $actionBtn;
                })
                ->setRowClass(function ($row) {
                    return 'sell_' . $row->sell_id;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }


    /*
     * Medicine list for add to invoice
     *
     * */
    public function medicine_list(Request $request)
    {

        if ($request->ajax()) {
            //$data = Medicine::latest()->get();
            $data = DB::table('medicines')->select(['id', 'name', 'packing', 'size', 'price']);
            //dd($data);
            return DataTables::of($data)
                ->addIndexColumn()

                ->addColumn('mergeColumn', function ($row) {
                    return $row->packing . ' ' . $row->size;
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="javascript:void(0);" onclick="add_product_to_chart(' . $row->id . ',this,' . $row->id . ')"><i class="fa fa-cart-plus" aria-hidden="true"></i></a>';
                    return $actionBtn;
                })
                ->setRowClass(function ($row) {
                    return "product_" . $row->id;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /*
     * Customer list for invoice
     *
     * */
    private function customer_list()
    {
        $customer = Customer::select('id', 'customer_name')->get();
        return $customer;
    }

    /*
     * Return representative list
     *
     * */

    private function representative_list(){

        $representative = Representative::select('id','name')->get();
        return $representative;
    }

    /*
     * Save all invoice data
     *
     * */


    public function save_invoice_data(Request $request)
    {
        //dd($request->all());
        $has_error = $this->validate_invoice_data($request->all());
        if ($has_error) {
            return redirect()->back()->withErrors($has_error);
        } else {
            $customer_id = $request->input('customer_id');
            $representative_id = $request->input('representative_id');
            $product_list = $request->input('product_id');
            $product_qty = $request->input('product_qty');
            $product_bonus = $request->input('product_bonus');
            $product_price = $request->input('product_price');
            $total_qty = $request->input('total_qty');
            $total_bonus = $request->input('total_bonus');
            $total_product_price = $request->input('total_product_price');
            $counter = 0;
            $total = 0;
            $invoice_no = date("Ymd") . '-' . date("his");

            foreach ($product_list as $product_id) {
                $price = $this->get_product_price($product_id);
                $qty = $product_qty[$counter];
                $total = $total + ($qty * $product_price[$counter]);
                $counter++;
            }
            if ($total != $total_product_price) {
                return redirect()->back()->withErrors("Input total price dosen't match");
            } else {
                DB::beginTransaction();
                try {
                    $sell = Sell::create([
                        'customer_id' => $customer_id,
                        'representative_id' => $representative_id,
                        'invoice_no' => $invoice_no,
                        'total_qty' => $total_qty,
                        'total_bonus' => $total_bonus,
                        'total_product_price' => $total_product_price,
                    ]);
                    $sell_id = $sell->id;
                    $counter = 0;
                    $insert_row = array();

                    try {

                        foreach ($product_list as $product_id) {
                            $insert_row[] = [
                                'product_id' => $product_id,
                                'product_qty' => $product_qty[$counter],
                                'product_bonus' => $product_bonus[$counter],
                                'product_price' => $product_price[$counter],
                                'customer_id' => $customer_id,
                                'representative_id' => $representative_id,
                                'sell_id' => $sell_id,
                                'status' => 1,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ];
                            $counter++;
                        }

                        try {
                            Invoice::insert($insert_row);
                            DB::commit();
                            return redirect()->back()->with('message', 'Invoice created successfully');
                        } catch (\Exception $e) {
                            DB::rollBack();
                            return redirect()->back()->withErrors("Invoice dosen't make complete" . $e->getMessage());
                        }

                    } catch (\Exception $e) {
                        DB::rollBack();
                        return redirect()->back()->withErrors("Invoice dosen't make complete" . $e->getMessage());
                    }

                } catch (\Exception $e) {
                    DB::rollBack();
                    return redirect()->back()->withErrors("Invoice dosen't make complete" . $e->getMessage());
                }
            }
        }
    }


    /*
     * Validation new invoice data
     *
     * */

    private function validate_invoice_data($request)
    {

        $validator = Validator::make($request, [
            'customer_id' => 'required|exists:customers,id',
            'product_id' => 'required|array',
            'product_qty' => 'required|array',
            'product_qty.*' => 'required|numeric',
            'product_bonus' => 'required|array',
            'product_price' => 'required|array',
            'total_qty' => 'required|numeric',
            'total_bonus' => 'required|numeric',
            'total_product_price' => 'required|numeric',
            'product_id.*' => 'required|exists:medicines,id'
        ], [
            'customer_id.exists' => 'Customer not found',
            'product_id.array' => 'Invalid Medicine list',
            'product_qty.array' => 'Invalid Medicine quantity',
            'product_id.*.exists' => 'Medicine not found',
            'product_qty.*.numeric' => 'Invalid Medicine qty data',
            'total_product_price.required' => 'Total price is required',
            'total_product_price.numeric' => 'Invalid total price',
        ]);

        if ($validator->fails()) {
            return $validator->errors()->all();
        } else {
            return false;
        }
    }

    /*
     * Get product price for match invoice product price
     *
     * */


    private function get_product_price($product_id)
    {

        return Medicine::where(['id' => $product_id])->select('id', 'price')->first();
    }


    /*
     * Invoice view
     *
     * */


    public function invoice_view($sell_id)
    {
        $sells_item = Sell::with(['customer', 'invoice'])->where('id', $sell_id)->first();
        //dd($sells_item);
        if ($sells_item) {
            return view('invoice.invoice-details', ['sells_item' => $sells_item]);
        } else {
            return redirect()->back()->withErrors('Sorry, Invoice not found');
        }
    }

    /*
     * Invoice PDF view
     *
     * */
    public function invoice_pdf_view($invoice_id)
    {

        $sells_item = Sell::with(['customer', 'invoice','representative'])->where('id', $invoice_id)->first();
        if ($sells_item) {

            $pdf = PDF::loadView('invoice.invoice-pdf', ['sells_item' => $sells_item]);
            $pdf->SetProtection(['copy', 'print'], '', 'pass');
            return $pdf->stream($sells_item->customer->customer_name . '.pdf');
        } else {
            return redirect()->back()->withErrors('Sorry, Invoice not found');
        }

    }


    /*
     * Print Invoice
     *
     * */

    public function get_invoice_print($invoice_id)
    {
        $sells_item = Sell::with(['customer', 'invoice','representative'])->where('id', $invoice_id)->first();
        if ($sells_item) {
            return view('invoice.print-invoice', ['sells_item' => $sells_item]);
            //return view('print.pages.invoice',['quotation'=>$sells_item]);
        } else {
            return redirect()->back()->withErrors('Sorry, Invoice not found');
        }
    }

    /*
     * View update information
     *
     * */
    public function invoice_update_view($sell_id)
    {
        $customers = $this->customer_list();
        $representative_list = $this->representative_list();
        $sells_item = Sell::with(['customer', 'invoice','representative'])->where('id', $sell_id)->first();
        //dd($sells_item);
        if ($sells_item) {
            return view('invoice.update-invoice', ['sells_item' => $sells_item, 'customers' => $customers,'representative_list'=>$representative_list]);
        } else {
            return redirect()->back()->withErrors('Sorry, Invoice not found');
        }
    }


    /*
     * Save update information
     *
     * */

    public function save_update_data(Request $request, $id)
    {

        /*$customer_id = $request->input('customer_id');
        $representative_id = $request->input('representative_id');
        $product_list = $request->input('product_id');
        $product_qty = $request->input('product_qty');
        $product_bonus = $request->input('product_bonus');
        $product_price = $request->input('product_price');
        $total_qty = $request->input('total_qty');
        $total_bonus = $request->input('total_bonus');
        $total_product_price = $request->input('total_product_price');
        $counter = 0;
        $total = 0;

        foreach ($product_list as $product_id) {
            $price = $this->get_product_price($product_id);
            $qty = $product_qty[$counter];
            $total = $total + ($qty * $product_price[$counter]);
            $counter++;
        }
        if ($total != $total_product_price) {
            return redirect()->back()->withErrors("Input total price dosen't match");
        } else {
            DB::beginTransaction();
            try {

                $sell_data = Sell::find($id);
                $sell_id = $sell_data->id;
                $sell_data->customer_id = $customer_id;
                $sell_data->representative_id = $representative_id;
                $sell_data->total_qty = $total_qty;
                $sell_data->total_bonus = $total_bonus;
                $sell_data->total_product_price = $total_product_price;
                $sell_data->save();

                $counter = 0;
                $insert_row = array();

                try {
                    //DB::table('invoices')->where('sell_id', $sell_id)->delete();
                    foreach ($product_list as $product_id) {
                        $insert_row[] = [
                            'product_id' => $product_id,
                            'product_qty' => $product_qty[$counter],
                            'product_bonus' => $product_bonus[$counter],
                            'product_price' => $product_price[$counter],
                            'customer_id' => $customer_id,
                            'representative_id' => $representative_id,
                            'sell_id' => $sell_id,
                            'status' => 1,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ];
                        $counter++;
                    }

                    try {
                        foreach($insert_row as $row){
                            Invoice::updateOrCreate(['sell_id'=>$row['sell_id'],'product_id'=>$row['product_id']], $row);
                        }
                        //Invoice::insert($insert_row);
                        DB::commit();
                        return redirect()->back()->with('message', 'Invoice update successfully');
                    } catch (\Exception $e) {
                        DB::rollBack();
                        return redirect()->back()->withErrors("Invoice dosen't updated" . $e->getMessage());
                    }

                } catch (\Exception $e) {
                    DB::rollBack();
                    return redirect()->back()->withErrors("Invoice dosen't make updated" . $e->getMessage());
                }

            } catch (\Exception $e) {
                DB::rollBack();
                return redirect()->back()->withErrors("Invoice dosen't make updated" . $e->getMessage());
            }
        }
    }*/

        $customer_id = $request->input('customer_id');
        $representative_id = $request->input('representative_id');
        $product_list = $request->input('product_id');
        $product_qty = $request->input('product_qty');
        $product_bonus = $request->input('product_bonus');
        $product_price = $request->input('product_price');
        $total_qty = $request->input('total_qty');
        $total_bonus = $request->input('total_bonus');
        $total_product_price = $request->input('total_product_price');
        $counter = 0;
        $total = 0;

        foreach ($product_list as $product_id) {
            $price = $this->get_product_price($product_id);
            $qty = $product_qty[$counter];
            $total = $total + ($qty * $product_price[$counter]);
            $counter++;
        }
        if ($total != $total_product_price) {
            return redirect()->back()->withErrors("Input total price dosen't match");
        } else {
            DB::beginTransaction();
            try {

                $sell_data = Sell::find($id);
                $sell_id = $sell_data->id;
                $sell_data->customer_id = $customer_id;
                $sell_data->representative_id = $representative_id;
                $sell_data->total_qty = $total_qty;
                $sell_data->total_bonus = $total_bonus;
                $sell_data->total_product_price = $total_product_price;
                $sell_data->save();

                $counter = 0;
                $insert_row = array();

                try {
                    DB::table('invoices')->where('sell_id', $sell_id)->delete();
                    foreach ($product_list as $product_id) {
                        $insert_row[] = [
                            'product_id' => $product_id,
                            'product_qty' => $product_qty[$counter],
                            'product_bonus' => $product_bonus[$counter],
                            'product_price' => $product_price[$counter],
                            'customer_id' => $customer_id,
                            'representative_id' => $representative_id,
                            'sell_id' => $sell_id,
                            'status' => 1,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ];
                        $counter++;
                    }

                    try {
                        Invoice::insert($insert_row);
                        DB::commit();
                        return redirect()->back()->with('message', 'Invoice update successfully');
                    } catch (\Exception $e) {
                        DB::rollBack();
                        return redirect()->back()->withErrors("Invoice dosen't updated" . $e->getMessage());
                    }

                } catch (\Exception $e) {
                    DB::rollBack();
                    return redirect()->back()->withErrors("Invoice dosen't make updated" . $e->getMessage());
                }

            } catch (\Exception $e) {
                DB::rollBack();
                return redirect()->back()->withErrors("Invoice dosen't make updated" . $e->getMessage());
            }
        }
    }

}
