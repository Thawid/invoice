<?php

namespace App\Http\Controllers;

use App\Models\Representative;
use Illuminate\Http\Request;
use DataTables;

class RepresentativeController extends Controller
{



   /*
    * Representative list
    *
    * */


    public function index(){
        return view('representative.index');
    }

    /*
     * Return representative data
     *
     * */


    public function get_representative_data(Request $request){
        if ($request->ajax()) {
            $data = Representative::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-warning btn-sm" href="javascript:void(0);" onclick="editRepresentative('.$row->id.');">
                                                                 Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteRepresentative('.$row->id.');">
                                                                Delete</a>
                                           </div>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }


    /*
     * Create new representative view
     *
     * */


    public function create_representative(){
        return view('representative.create');
    }


    /*
     * Store representative data
     *
     * */

    public function store_representative_data(Request $request){
        //dd($request->all());

        $request->validate([
            'name'=>'required|unique:representatives',
            'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:14|unique:representatives',
            'zone'=>'required',
        ]);

        $input = $request->all();
        Representative::create($input);
        return back()->with('success','Representative create successfully');
    }


    /*
     * Update representative by id
     *
     * */

    public function get_representative_by_id($id){
        $representative = Representative::find($id);
        return response()->json($representative);
    }

    public function update_representative(Request $request){
        $request->validate([
            'name'=>'required',
            'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:14',
            'zone'=>'required',
        ]);

        $representative = Representative::find($request->id);
        $representative->name = $request->name;
        $representative->phone = $request->phone;
        $representative->zone = $request->zone;
        $representative->save();
        $message = "Data update successfully";
        return response()->json(['message' => $message], 200);
    }


    /*
     * Delete representative data
     * */

    public function delete_representative($id){

        $representative = Representative::find($id);
        $representative->delete();
        return response()->json(['message'=>'Data delete successfully']);
    }

}
