<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SummaryReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){
        return view('report.summary-report');
    }


    public function  get_summary_report(Request $request){
        $start_date = $request->start_date;
        $start_date = $start_date. ' 00:00:00';
        $end_date = $request->end_date;
        if(empty($end_date)){
            $end_date = $request->start_date;
        }
        $end_date = $end_date. ' 23:59:59';
       $summary_reports = DB::table('invoices')
           ->join('medicines','medicines.id','invoices.product_id')
           ->select(DB::raw('medicines.name,medicines.packing,medicines.size,SUM(invoices.product_qty) as qty, SUM(invoices.product_bonus) as bonus, medicines.price'))
           ->whereBetween('invoices.created_at',[$start_date,$end_date])
           ->groupBy('invoices.product_id')
           ->get();

       return view('report.summary-report',compact('summary_reports'));

    }
}
