<?php

namespace App\Http\Controllers;

use App\Models\Representative;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RepresentativeReportController extends Controller
{


    public function index()
    {
        $representative_data = Representative::select('id', 'name')->get();
        return view('report.representative-report', compact('representative_data'));
    }


    public function get_representative_report(Request $request)
    {

        $id = $request->data['representative_id'];
        $start_date = $request->data['start_date'];
        $start_date = $start_date . ' 00:00:00';
        $end_date = $request->data['end_date'];

        $date_one = date_create($start_date);
        $show_date_one = bangla(date_format($date_one, 'd-m-Y'));
        if (empty($end_date)) {
            $show_date_two = '';
        } else {
            $date_two = date_create($end_date);
            $show_date_two = bangla(date_format($date_two, 'd-m-Y'));
        }
        if (empty($end_date)) {
            $end_date = $request->data['start_date'];
        }
        $end_date = $end_date . ' 23:59:59';




        $representative = Representative::where('id', $id)->first();

        $report_data = DB::table('invoices')
            ->join('medicines', 'medicines.id', 'invoices.product_id')
            ->select(DB::raw('medicines.name,medicines.packing,medicines.size,SUM(invoices.product_qty) as qty, SUM(invoices.product_bonus) as bonus, medicines.price'))
            ->whereBetween('invoices.created_at', [$start_date, $end_date])
            ->where('invoices.representative_id', $id)
            ->groupBy('invoices.product_id')
            ->get();
        return view('report.representative-report-view', compact('representative', 'report_data', 'show_date_one', 'show_date_two'));
    }
}
