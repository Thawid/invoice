<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DueReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /*
     * view due report
     *
     * */

    public function index()
    {

        return view('report.due-report');
    }

    /*
     * Ger due report
     *
     * */

    public function get_due_report(Request $request)
    {

        //dd($request->all());
        $start_date = $request->start_date;
        $start_date = $start_date. ' 00:00:00';
        $end_date = $request->end_date;
        if(empty($end_date)){
            $end_date = $request->start_date;
        }
        $end_date = $end_date. ' 23:59:59';

        $data = DB::table('customers')
            ->join('sells','sells.customer_id','=','customers.id')
            ->select('customers.customer_name','sells.invoice_no','sells.total_product_price','sells.created_at')
            ->whereBetween('sells.created_at',[$start_date,$end_date])
            ->get();

        return view('report.due-report',compact('data'));



    }
}
