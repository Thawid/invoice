<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    use HasFactory;

    protected $table = "sells";

    protected $fillable = ['customer_id','invoice_no','total_qty','total_bonus','total_product_price'];

    /*
     * Every sell has one customer
     *
     * */

    public function customer(){
        return $this->hasOne('App\Models\Customer','id','customer_id')->select(['id','customer_name','address','phone']);
    }

    /*
     * Every sell has may transaction
     *
     * */

    public function invoice(){
        return $this->hasMany('App\Models\Invoice','sell_id','id')->with(['medicine']);
    }


    /*
     * Get invoice representative data
     *
     * */

    public function representative(){
        return $this->hasOne('App\Models\Representative','id','representative_id')->select(['id','name','phone','zone']);
    }
}
