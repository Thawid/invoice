@extends('layout.main')
@section('style')

@endsection
@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">সকল কাস্টমারের তালিকা</h1>
                    <a href="{{url('customer-create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="bi bi-people-fill"></i> নতুন কাস্টমার </a>
                </div>
            </div>
            <!-- page header -->



            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered yajra-datatable" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>কাস্টমার নাম</th>
                                    <th>মোবাইল নম্বর</th>
                                    <th>ঠিকানা</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->

            <!--  customer Modal Edit -->
            <div class="modal fade" id="customerModalEdit">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="customerModalEdit">তথ্যগুলো পরিবর্তন করুন</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="update_customer">
                                @csrf
                                <input type="hidden" id="id" name="id">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="customer_name">কাস্টমার নাম * </label>
                                        <input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="মোহাম্মাদ মেহেদী ইসলাম" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="phone">মোবাইল নম্বর * </label>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="০১৬৭৬৯৬৬২৬০" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address">ঠিকানা * </label>
                                    <textarea class="form-control" id="address" name="address" placeholder="আপনার কাস্টমারে ঠিকানা এখানে লিখুন" rows="3"
                                    ></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End customer Modal Edit -->
            <!--  customer Modal Delete -->
            <div class="modal fade" id="customerModalDelete" tabindex="-1" role="dialog" aria-labelledby="customerModalDelete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="customerModalDelete">তথ্যটি মুছ</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            আপনি কি এই তথ্যটি মুছতে চান?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-warning" data-dismiss="modal">না</button>
                            <button type="button" class="btn bg-info">হ্যাঁ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {

            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('customer.list') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'customer_name', name: 'customer_name'},
                    {data: 'phone', name: 'phone'},
                    {data: 'address', name: 'address'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

        });
    </script>
    <script>
        function editCustomer(id) {
            $.get('customer/'+id,function(customer) {
                $("#id").val(customer.id);
                $("#customer_name").val(customer.customer_name);
                $("#phone").val(customer.phone);
                $("#address").val(customer.address);
                $("#customerModalEdit").modal('toggle');
            });
        }

        $("#update_customer").submit(function(e){

            e.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            let id = $("#id").val();
            let customer_name = $("#customer_name").val();
            let phone = $("#phone").val();
            let address = $("#address").val();
            let _token = $("input[name= _token]").val();
            $.ajax({
                url :"{{route('update.customer')}}",
                type:"PUT",
                data:{
                    id:id,
                    customer_name:customer_name,
                    phone:phone,
                    address:address,
                    _token:_token,
                },
                success:function (response) {
                    if(response){
                        toastr.success(response.message);
                        $("#customerModalEdit").modal('toggle');
                        $("#update_customer")[0].reset();
                        $('.yajra-datatable').DataTable().ajax.reload();

                    }

                },
                error: function(err){
                    toastr.error("Something went wrong !");
                    $("#customerModalEdit").modal('toggle');
                    $("#update_customer")[0].reset();
                }
            });
        });

        function deleteCustomer(id){
            if(confirm('Do you realy want to delete this record?')){
                $.ajax({
                    url:'delete-customer/'+id,
                    type:'get',
                    data:{
                        _token:$("input[name=_token]").val()
                    },
                    success:function(response){
                        toastr.success(response.message);
                        $('.yajra-datatable').DataTable().ajax.reload();
                    },
                    error:function (err){
                        toastr.error("Data can't be deleted !");

                    }
                });
            }
        }
    </script>

@endsection
