@extends('layout.main')
@section('body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">নতুন কাস্টমার</h1>
                    <a href="{{route('customers')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="bi bi-people-fill"></i> সকল কাস্টমারের তালিকা </a>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success" id="alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> {{ $message }}
                    </div>
                @endif
                @if($errors->any())
                    <div class="alert alert-danger" id="alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Warning!</strong> {{ $error }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <p>
                            <small><i>* দেওয়া প্রতিটি ঘরের তথ্য সঠিকভাবে প্রদান করতে হবে</i></small>
                        </p>
                        <form  action="{{route('save.customer')}}" method="post">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="customerName">কাস্টমার নাম * </label>
                                    <input type="text" class="form-control" id="customerName" name="customer_name" placeholder="নাম" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="customerMobile">মোবাইল নম্বর * </label>
                                    <input type="number" class="form-control" id="customerMobile" name="phone" placeholder="০১৬৭৬০০০০০০" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="customerAddress">ঠিকানা * </label>
                                <textarea class="form-control" id="customerAddress" name="address" placeholder="আপনার কাস্টমারে ঠিকানা এখানে লিখুন
"  rows="3"></textarea>
                            </div>
                            <button type="submit" id="submit" class="btn bg-success btn-sm"> + তথ্য যোগ করুন</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
        </div>
    </div>

@endsection

@section('script')
    <script>

        $("#alert-success").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert-success").slideUp(500);
        });

        $("#alert-danger").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert-danger").slideUp(500);
        });

    </script>
@endsection
