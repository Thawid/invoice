@extends('layout.main')

@section('body')
    <div class="container-fluid">
        <h1 class="mt-4">ড্যাশবোর্ড</h1>

        <div class="card mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-warning text-white mb-4 text-center">
                            <div class="card-body">
                                <h5>নতুন কাস্টমার</h5>
                                <h3>@if(isset($new_customer)) {{bangla($new_customer->new_customer)}} @endif জন</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-primary text-white mb-4 text-center">
                            <div class="card-body">
                                <h5>মোট ওষুধ</h5>
                                <h3>@if(isset($total_medicine)) {{en2bnNumber($total_medicine->total_medicine)}} @endif টি</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-success text-white mb-4 text-center">
                            <div class="card-body">
                                <h5>মোট কাস্টমার</h5>
                                <h3>@if(isset($total_customer)) {{en2bnNumber($total_customer->total_customer)}} @endif জন</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-info text-white mb-4 text-center">
                            <div class="card-body">
                                <h5>আজকের বিল</h5>
                                <h3>@if(isset($today_bill)) {{en2bnNumber($today_bill->today_bill)}} @endif টাকা</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered due-report" id='medicineTable'>
                            <thead>
                            <tr>
                                <th>নং</th>
                                <th>সময় </th>
                                <th>আই পি </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $count = 1;
                            @endphp
                            @if(isset($login_info))
                                @foreach($login_info as $info)
                                    <tr>
                                        <td>{{$count++}}</td>
                                        <td>{{$info->last_login_at}}</td>
                                        <td>{{$info->last_login_ip}}</td>

                                    </tr>

                                @endforeach

                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
