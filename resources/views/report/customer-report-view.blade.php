<div id="printableArea">

    <div class="th-table-header">
        <table>
            <tr>
                <td><h1>হামদর্দ ল্যাবরেটরীজ (ওয়াক্ফ) বাংলাদেশ</h1></td>
            </tr>
            <tr>
                <td><p>রুপায়ন ট্রেড সেন্টার (১৩-১৪ তলা ),কাজী নজরুল ইসলাম এভিনিউ-বাংলামোটর,ঢাকা-১০০০</p></td>
            </tr>
            <tr>
                <td><h3>পরিবেশক : মনি স্টোর ফার্মেসি,হামদর্দ - হাটহাজারী শাখা চট্টগ্রাম , মোবাইল : ০১৮৫৮০৫৮৭৬০, ০১৩১৩৭৮১০৮১</h3></td>
            </tr>

        </table>
    </div>
    <div class="th-table-info">
        <table>
            <tr>

                <td style="width: 80px;"><p>পার্টি নাম : </p></td>
                <td><p>@if(isset($customer)) {{$customer->customer_name}} @endif  </p></td>
                <td style="width: 80px;"><p>তারিখ : </p></td>
                <td><p>@if(isset($show_date_one)) {{ $show_date_one }} @endif - @if(isset($show_date_two)) {{ $show_date_two }} @endif </p></td>

            </tr>
            <tr>

                <td style="width: 80px;"><p>মোবাইল :</p></td>
                <td><p> @if(isset($customer)) {{bangla($customer->phone)}} @endif </p></td>
                <td style="width: 80px;"><p> ঠিকানা:</p></td>
                <td><p> @if(isset($customer)) {{$customer->address}} @endif  </p></td>
            </tr>

        </table>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered" id='medicineTable'>
            <thead>
            <tr>
                <th>ক্রম</th>
                <th>পণ্যের নাম</th>
                <th>সাইজ</th>
                <th>পরিমাণ</th>
                <th>বোনাস</th>
                <th>দাম</th>
                <th>মোট</th>
            </tr>
            </thead>
            <tbody>
            @php
                $count = 1;
            @endphp
            @if(isset($customer_report))
                @foreach($customer_report as $report)
                    <tr>
                        <td>{{ en2bnNumber($count++) }}</td>
                        <td>{{ $report->name }}</td>
                        <td>{{ bangla($report->packing) .' '. $report->size }}</td>
                        <td>{{ en2bnNumber($report->qty) }}</td>
                        <td>{{ en2bnNumber($report->bonus) }}</td>
                        <td>{{ en2bnNumber($report->price) }}</td>
                        <td>{{ en2bnNumber($report->qty * $report->price) }}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>

</div>
<div class="from-group">
    <button class="btn btn-primary mb-2" onclick="printDiv('printableArea')">তথ্যগুলো
        প্রিন্ট করুন
    </button>
</div>

