    @extends('layout.main')

    @section('style')

        <style>
            /*body {
                margin: 0 auto;
                font-family: "SolaimanLipi", Arial, sans-serif !important;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: #212529;
                background-color: #fff;
                padding: 0;
            }*/
            @media print {
                @page { margin: 0; }

            }
            #printableArea {
                margin-top: 20px;
                margin-left: 10px;
                margin-right: 10px;
            }
            table {
                font-family: "SolaimanLipi", Arial, sans-serif !important;
                text-align: center;
                margin: 0;
                padding: 0;
                width: 100%;
            }

            .th-table-header, .th-table-info, .th-table-body {
                margin-bottom: 20px;
            }

            .th-table-header table tr td h1 {
                margin: 0;
                font-size: 20px;
            }

            .th-table-header table tr td h2 {
                margin: 0;
                font-size: 18px;
            }

            .th-table-header table tr td h3 {
                padding: 0;
                margin: 0;
                font-size: 16px;
            }

            .th-table-info p {
                margin: 0;
            }

            .th-table-info table tr td p {
                text-align: left;
            }

            .th-table-header table tr td p {
                padding: 0;
                margin: 0;
                font-size: 16px;
            }

            .th-table-body {
                width: 100%;
                text-align: center;
                border: 1px solid #ddd;
            }

            .th-table-body thead {
                border: 1px solid #ddd;
                background-color: #ddd;
            }

            .th-table-body tbody tr td {
                border: 1px solid #ddd;

            }

            .th-table-signature {
                margin-top: 40px;
            }
        </style>
    @endsection

    @section('body')
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 mt-lg-4 mt-4">
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0">বিক্রয় সামারি রিপোর্ট</h1>
                        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                                class="bi bi-people-fill"></i> ঔষধের তালিকা </a>
                    </div>
                </div>
                <!-- page header -->
                <div class="col-md-12">
                    <form class="form-row" action="{{route('summary.report')}}" method="post">
                        @csrf
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="date" class="form-control" name="start_date" id="start_date">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="date" class="form-control" name="end_date" id="end_date">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="from-group">
                                <input type='submit' class="btn btn-primary" value='Search' id='btnSearch'>
                            </div>
                        </div>
                    </form>
                    <div class="" id="printableArea">
                        <div class="th-table-header mb-4 mt-4">
                            <table>
                                <tr>
                                    <td><h1>হামদর্দ ল্যাবরেটরীজ (ওয়াক্ফ) বাংলাদেশ</h1></td>
                                </tr>
                                <tr>
                                    <td><p>রুপায়ন ট্রেড সেন্টার (১৩-১৪ তলা ),কাজী নজরুল ইসলাম এভিনিউ-বাংলামোটর,ঢাকা-১০০০</p></td>
                                </tr>
                                <tr>
                                    <td><h3>পরিবেশক : মনি স্টোর ফার্মেসি,হামদর্দ - হাটহাজারী শাখা চট্টগ্রাম , মোবাইল : ০১৮৫৮০৫৮৭৬০, ০১৩১৩৭৮১০৮১</h3></td>
                                </tr>

                            </table>
                        </div>

                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered summary-report" id='summary-report'>
                                        <thead>
                                        <tr>
                                            <th>ক্রম</th>
                                            <th>পণ্যের নাম</th>
                                            <th>সাইজ</th>
                                            <th>পরিমাণ</th>
                                            <th>বোনাস</th>
                                            <th>দাম</th>
                                            <th>মোট</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $count = 1;
                                        @endphp
                                        @if(isset($summary_reports))
                                            @foreach($summary_reports as $summary)
                                                <tr>
                                                    <td>{{en2bnNumber($count++)}}</td>
                                                    <td>{{$summary->name}}</td>
                                                    <td>{{en2bnNumber($summary->packing) .' '. $summary->size}}</td>
                                                    <td>{{en2bnNumber($summary->qty)}}</td>
                                                    <td>{{en2bnNumber($summary->bonus)}}</td>
                                                    <td>{{en2bnNumber($summary->price)}}</td>
                                                    <td>{{en2bnNumber($summary->qty * $summary->price)}}</td>
                                                </tr>

                                            @endforeach

                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary mb-2" onclick="printDiv('printableArea')">তথ্যগুলো
                            প্রিন্ট করুন
                        </button>
                    </div>
                </div>
                <!-- end body content col-md-12 -->


            </div>
        </div>
    @endsection


    @section('script')
        <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('#summary-report').DataTable({
                    "oLanguage": {
                        sProcessing: "<img src='{{asset('loader/preloader.gif')}}'>"
                    },
                    "processing": true,
                    "searching": false,
                    "paging": false,
                    "ordering": false,
                    "info": false,

                });

            });


            function printDiv(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;

                document.body.innerHTML = printContents;

                window.print();

                document.body.innerHTML = originalContents;
            }
        </script>
    @endsection
