@extends('layout.main')

@section('style')
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet"/>
    <style>
        #medicineTable th {
            text-align: center;
        }
    </style>
@endsection


@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">ব্যক্তির ক্রয় রিপোর্ট</h1>
                    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                            class="bi bi-people-fill"></i> ঔষধের তালিকা </a>
                </div>
            </div>
            <!-- page header -->
            <div class="col-md-3">
                <div class="form-group">
                    <select class="form-control customer-list" name="search" id="search">
                        <option value="0">Select Customer</option>
                        @foreach($customers as $customer)
                            <option value="{{$customer->id}}">{{ $customer->customer_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="date" class="form-control" name="start_date" id="start_date">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="date" class="form-control" name="end_date" id="end_date">
                </div>
            </div>
            <div class="col-md-3">
                <div class="from-group">
                    <input type='button' class="btn btn-primary" value='Search' id='btnSearch'>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id='medicineTable'>
                                <thead>
                                <tr>
                                    <th>ক্রম</th>
                                    <th>পণ্যের নাম</th>
                                    <th>সাইজ</th>
                                    <th>পরিমাণ</th>
                                    <th>বোনাস</th>
                                    <th>দাম</th>
                                    <th>মোট</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="from-group">
                    <input type='button' class="btn btn-primary" value='Print' id='printSearch'>
                </div>
            </div>
            <!-- end body content col-md-12 -->

        </div>
    </div>
@endsection


@section('script')
    <script src="{{asset('assets/js/select2.min.js')}}"></script>

    <script type='text/javascript'>
        $(document).ready(function () {

            // use select 2 for customer

            $('.customer-list').select2();


            // Search by customer id
            $('#btnSearch').click(function () {
                var userid = $('#search').val().trim();
                var start_date = $('#start_date').val().trim();
                var end_date = $('#end_date').val().trim();
                if (userid > 0) {
                    fetchRecords(userid, start_date, end_date);
                }
            });
        });

        function fetchRecords(id, start_date, end_date) {
            $.ajax({
                url: 'get_data/' + id + '/' + start_date + '/' + end_date,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    var len = 0;
                    $('#medicineTable tbody').empty(); // Empty <tbody>
                    if (response['data'] != null) {
                        len = response['data'].length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response['data'][i].id;
                            var name = response['data'][i].name;
                            var packing = response['data'][i].packing;
                            var size = response['data'][i].size;
                            var qty = response['data'][i].qty;
                            var bonus = response['data'][i].bonus;
                            var price = response['data'][i].price;

                            var tr_str = "<tr>" +
                                "<td align='center'>" + (i + 1) + "</td>" +
                                "<td align='center'>" + name + "</td>" +
                                "<td align='center'>" + packing + ' ' + size + "</td>" +
                                "<td align='center'>" + qty + "</td>" +
                                "<td align='center'>" + bonus + "</td>" +
                                "<td align='center'>" + price + "</td>" +
                                "<td align='center'>" + qty * price + "</td>" +
                                "</tr>";
                            $("#medicineTable tbody").append(tr_str);
                        }
                    } else {
                        var tr_str = "<tr>" +
                            "<td align='center' colspan='7'>No record found.</td>" +
                            "</tr>";
                        $("#medicineTable tbody").append(tr_str);
                    }
                }
            });
        }
    </script>





@endsection
