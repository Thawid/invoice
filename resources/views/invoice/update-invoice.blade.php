@extends('layout.main')

@section('style')
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet"/>
    <style>
        .product_list th{
            font-size: 13px !important;
        }
        .product_list  td{
            font-size: 13px !important;
        }
    </style>
@endsection

@section('body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">সবগুলো ইনভেস্ট</h1>
                    <a href="{{url('create-invoice')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> নতুন ইনভয়েস তৈরি করুন </a>
                </div>
            </div>
            <!-- page header -->
            <div class="col-md-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered medicine-list" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead>
                                <tr>
                                    <th>ওষুধের নাম</th>
                                    <th>পরিবেশনা</th>
                                    <th>দাম</th>
                                    <th>যোগ</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="{{route('update.invoice.data',$sells_item->id)}}" method="post">
                            @csrf
                            <div class="form-row align-items-center">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select required name="customer_id" class="custom-select mr-sm-2 customer-list" data-live-search="true" id="customer_id" data-live-search-style="begins" title="Select customer...">
                                            @foreach($customers as $customer)
                                                <option value="{{$customer->id}}" @if($sells_item->customer_id == $customer->id)  selected @endif >{{$customer->customer_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select required name="representative_id" class="custom-select mr-sm-2 representative-list" data-live-search="true" id="representative_id" data-live-search-style="begins" title="Select customer...">
                                            @foreach($representative_list as $representative)
                                                <option value="{{$representative->id}}" @if($sells_item->representative_id == $representative->id)  selected @endif >{{$representative->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered update-invoice product_list"  width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>পণ্যের নাম</th>
                                        <th>সাইজ </th>
                                        <th>পরিমাণ </th>
                                        <th>বোনাস  </th>
                                        <th>দাম </th>
                                        <th>সর্বমোট </th>
                                        <th>Action </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $counter = 1;
                                        $product_qty = 0;
                                        $total_price =0;
                                        $total_bonus = 0;
                                    @endphp
                                    @if(isset($sells_item))

                                    @endif
                                    @foreach($sells_item->invoice as $invoice)
                                        @php
                                            $total_price = $total_price+$invoice->product_price*$invoice->product_qty;
                                            $product_qty = $product_qty+$invoice->product_qty;
                                            $total_bonus = $total_bonus+$invoice->product_bonus;
                                        @endphp
                                        <tr>
                                            <td>{{$invoice->medicine->name}}</td>
                                            <td>{{$invoice->medicine->packing .' '. $invoice->medicine->size}}</td>
                                            <td><input style="width:50px" type="text" name="product_qty[]" onkeyup="claculate_qty(this)" class="form-control single_qty qty-input product_qty" value="{{$invoice->product_qty}}" required></td>
                                            <td><input style="width:50px" type="text" name="product_bonus[]" onkeyup="claculate_bonus(this)" class="form-control single_bonus qty-input product_bonus" value="{{$invoice->product_bonus}}" required></td>
                                            <td><input style="width:100px" type="text" name="product_price[]" onkeyup="calculate_total()" class="form-control chart_product_price" value="{{$invoice->product_price}}" readonly></td>
                                            <td class="sub_total"> {{ $invoice->product_qty *  $invoice->product_price}}</td>

                                            <td><button type="button" class="ibtnDel btn btn-md btn-danger btn-sm"><i class="far fa-window-close"></i></button></td>
                                            <input type="hidden" name="id[]" value="{{$invoice->id}}">
                                            <input type="hidden" name="sell_id" value="{{$invoice->sell_id}}">
                                            <input type="hidden" name="product_id[]" value="{{$invoice->product_id}}">
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                                <table class="table thtablefooter">
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td>সর্বমোট</td>
                                        <td>:</td>
                                        <td style="text-align: right;"><input style="width: 80px;" type="text" name="total_qty" class="form-control total_qty" id="total_qty" value="{{$product_qty}}" readonly></td>
                                        <td style="text-align: right;"><input type="text" style="width: 80px;" class="form-control total_bonus" id="total_bonus" name="total_bonus" value="{{ $total_bonus }}" readonly></td>

                                        <td></td>
                                        <td style="text-align: left;"><input type="text" style="width: 80px;" class="form-control total_sum" id="total_product_price" name="total_product_price" value="{{$total_price}}" readonly></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">+ Update Data</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
        </div>
    </div>
@endsection


@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>
    <script>
        $(function () {

            var table = $('.medicine-list').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('medicine.list') }}",
                columns: [
                    {data: 'name', name: 'name', className: 'product_name'},
                    {
                        data: 'mergeColumn',
                        name: 'mergeColumn',
                        searchable: false,
                        sortable: false,
                        visible: true,
                        className: 'product_size'
                    },
                    {data: 'price', name: 'price', className: 'product_price'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'packing', name: 'packing', searchable: true, sortable: true, visible: false},
                    {data: 'size', name: 'size', searchable: true, sortable: true, visible: false},
                ]
            });

        });
        $(document).ready(function () {
            $('.customer-list').select2();
            $('.representative-list').select2();
        });
    </script>
    <script>

        var product_name = '';
        var product_size = '';
        var product_price = '';
        var amount = '';

        var product_qty = [];
        var product_price = [];
        var product_bonus = [];

        $(document).ready(function () {
            $('.customer-list').select2();
        });

        //Delete product temporary
        $("table.update-invoice tbody").on("click", ".ibtnDel", function(event) {
            rowindex = $(this).closest('tr').index();
            product_price.splice(rowindex, 1);
            product_qty.splice(rowindex, 1);
            product_bonus.splice(rowindex, 1);
            $(this).closest("tr").remove();
            calculate_total();
        });

        function get_product_details(current) {

            product_name = $(current).parent().parent().find(".product_name").text();
            product_size = $(current).parent().parent().find(".product_size").text();
            product_price = $(current).parent().parent().find(".product_price").text();

        }

        function add_product_to_chart(product_id, context, product_id) {

            get_product_details(context);
            product_price = product_price.replace(/[^0-9]/gi, '');
            var price_input = '<input style="width:100px" type="text" name="product_price[]" value="' + product_price + '" onkeyup="calculate_total()" class="form-control chart_product_price" readonly>';

            var row = '<tr><td class="single_name">' + product_name + '</td><td class="single_size">' + product_size + '</td>\n' +
                '<input name="product_id[]" type="hidden" value="' + product_id + '" class="">\n' +
                '<td><input style="width:50px" name="product_qty[]" onkeyup="claculate_qty(this)" type="text" class="form-control single_qty qty-input product_qty" placeholder="" required></td>\n' +
                '<td><input style="width:50px" name="product_bonus[]" onkeyup="claculate_bonus(this)" type="text" class="form-control single_bonus qty-input product_bonus" required></td>\n' +
                '<td  class="current_product_price">' + price_input + '</td>\n' +
                '<td class="sub_total">0 tk</td>\n' +
                '<td><a class="btn btn-md btn-danger btn-sm" href="javascript:void(0);"><i onclick="back_product_to_list(' + product_id + ',this,' + product_id + ')" class="far fa-window-close"></i></a></td></tr>';
            $(".product_list").append(row);
            $(context).parent().parent().hide();
        }

        //return product to productlist from chart
        function back_product_to_list(product_id, context, product_id) {
            $(".product_" + product_id).show();
            $(context).parent().parent().parent().remove();

        }

        //calculate quantity and price of product
        function claculate_qty(context) {
            var price = $(context).parent().parent().find(".current_product_price").text();

            var qty = context.value;
            price = price.substring(1);
            $(context).parent().parent().find(".sub_total").text(qty * price);
            calculate_total();
        }

        // calculate product bonous

        function claculate_bonus(context) {
            var bonus = 0;
            bonus = context.value;
            calculate_total();
        }

        //calculate total sub of product
        function calculate_total() {
            var sum = 0;
            var total_qty = 0;
            var total_bonus = 0;
            var qty_chart = 0;
            var qty_bonus = 0;

            $(".chart_product_price").each(function () {
                var val = $.trim($(this).val());
                qty_chart = $(this).parent().parent().find('.product_qty').val();
                qty_bonus = $(this).parent().parent().find('.product_bonus').val();
                var qty_price = (qty_chart > 0) ? qty_chart * val : 0;
                $(this).parent().parent().find('.sub_total').text(qty_price);
                $(this).parent().parent().find('.total_bonus').text(qty_bonus);
                sum += qty_price;
                total_qty = parseInt(qty_chart) + parseInt(total_qty);
                total_bonus = parseInt(qty_bonus) + parseInt(total_bonus);
            });

            $('.total_sum').text(sum);
            $('.total_qty').text(total_qty);
            $('.total_bonus').text(total_bonus);
            $('#total_product_price').val(sum);
            $('#total_bonus').val(total_bonus);
            $('#total_qty').val(total_qty);
        }
    </script>
    <script>

        @if(Session::has('message'))
        toastr.success("{{Session::get('message')}}");
        @endif

        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        toastr.error("{{ $error }}");
        @endforeach
        @endif

    </script>

@endsection
