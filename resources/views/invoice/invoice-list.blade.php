@extends('layout.main')

@section('style')
@endsection

@section('body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">সবগুলো ইনভেস্ট</h1>
                    <a href="{{url('create-invoice')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> নতুন ইনভয়েস তৈরি করুন </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered invoice-list" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>ইনভেস্ট নং</th>
                                    <th>তারিখ </th>
                                    <th>ক্রেতার নাম </th>
                                    <th>প্রতিনিধির নাম </th>
                                    <th>পরিমাণ  </th>
                                    <th>বোনাস  </th>
                                    <th>দাম </th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>নং</th>
                                    <th>ইনভেস্ট নং</th>
                                    <th>তারিখ </th>
                                    <th>ক্রেতার নাম </th>
                                    <th>প্রতিনিধির নাম </th>
                                    <th>পরিমাণ  </th>
                                    <th>বোনাস  </th>
                                    <th>সর্বমোট দাম </th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </tfoot>
                                <tbody>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->


            <!--  customer Modal Delete -->
            <div class="modal fade" id="medicinModalDelete" tabindex="-1" role="dialog" aria-labelledby="medicinModalDelete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="medicinModalDelete">তথ্যটি মুছ</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            আপনি কি এই তথ্যটি মুছতে চান?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-warning" data-dismiss="modal">না</button>
                            <button type="button" class="btn bg-info">হ্যাঁ</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end customer Modal Delete -->
        </div>
    </div>

@endsection


@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {

            var table = $('.invoice-list').DataTable({
                order: [[ 2, "desc" ]],
                processing: true,
                serverSide: true,
                ajax: "{{ route('get.invoice.list') }}",
                columns: [

                    {data: 'rownum', name: 'rownum', orderable: false, searchable: false},
                    {data: 'invoice_no', name: 'sells.invoice_no'},
                    {data: 'created_at', name: 'sells.created_at'},
                    {data: 'customer_name', name: 'customers.customer_name'},
                    {data: 'rp_name', name: 'representatives.name'},
                    {data: 'total_qty', name: 'sells.total_qty'},
                    {data: 'total_bonus', name: 'sells.total_bonus'},
                    {data: 'total_product_price', name: 'sells.total_product_price'},

                    {data: 'action', name: 'action', orderable: false, searchable: false },
                    {data: 'id', name: 'sell_id',searchable: false, sortable: false, visible: false},
                ]
            });

        });
    </script>
@endsection
