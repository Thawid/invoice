@extends('layout.main')
@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">নতুন ওষুধ বা পণ্য যুক্ত করুন</h1>
                    <a href="{{url('medicinelist')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="bi bi-people-fill"></i> ঔষধের তালিকা </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <p>
                            <small><i>* দেওয়া প্রতিটি ঘরের তথ্য সঠিকভাবে প্রদান করতে হবে</i></small>
                        </p>
                        <form  action="{{route('save.medicine')}}" method="post">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="medicineName">ওষুধের নাম * </label>
                                    <input type="text" class="form-control" id="medicineName" name="name" placeholder="সিনকরা" />
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="medicinePacking">প্যাকিং * </label>
                                    <input type="text" class="form-control" id="medicinePacking" name="packing" placeholder="৬০০" />
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="medicineSize"> সাইজ * </label>
                                    <select class="form-control" name="size" id="medicineSize">
                                        <option value="">Select Size</option>
                                        <option value="মি.লি">মি.লি</option>
                                        <option value="গ্রাম">গ্রাম</option>
                                        <option value="ক্যাপসুল">ক্যাপসুল</option>
                                        <option value="ট্যাবলেট">ট্যাবলেট</option>
                                        <option value="সেমিসলিড">সেমিসলিড </option>
                                        <option value="স্যাচেট">স্যাচেট</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="medicinePrice">এম,আর,পি - দাম * </label>
                                    <input type="number" class="form-control" name="price" id="medicinePrice" placeholder="১৬৭" />
                                </div>
                            </div>

                            <button type="submit" class="btn bg-success "> + ওষুধ যোগ করুন</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
        </div>
    </div>
@endsection

@section('script')
    <script>

        @if(Session::has('success'))
           toastr.success("{{Session::get('success')}}");
        @endif

        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
             toastr.error("{{ $error }}");
            @endforeach
        @endif

    </script>
@endsection
