@extends('layout.main')
@section('style')

@endsection
@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">সকল প্রতিনিধির তালিকা</h1>
                    <a href="{{url('create-representative')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="bi bi-people-fill"></i> নতুন প্রতিনিধি </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered representative-list" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>প্রতিনিধির নাম</th>
                                    <th>মোবাইল নম্বর</th>
                                    <th>জোন</th>
                                    <th>অন্যান্য</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->

            <!--  customer Modal Edit -->
            <div class="modal fade" id="representativeModalEdit">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="representativeModalEdit">তথ্যগুলো পরিবর্তন করুন</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="update_representative">
                                @csrf
                                <input type="hidden" id="id" name="id">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="name">প্রতিনিধির  নাম * </label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="মোহাম্মাদ মেহেদী ইসলাম" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="phone">মোবাইল নম্বর * </label>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="০১৬৭৬৯৬৬২৬০" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="zone">জোন * </label>
                                    <textarea class="form-control" id="zone" name="zone" placeholder="" rows="3"
                                    ></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">বাতিল করুন</button>
                                    <button type="submit" class="btn bg-success">পরিবর্তন গুলোর সংরক্ষন</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End customer Modal Edit -->
            <!--  customer Modal Delete -->
            <div class="modal fade" id="customerModalDelete" tabindex="-1" role="dialog" aria-labelledby="customerModalDelete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="customerModalDelete">তথ্যটি মুছ</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            আপনি কি এই তথ্যটি মুছতে চান?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-warning" data-dismiss="modal">না</button>
                            <button type="button" class="btn bg-info">হ্যাঁ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {

            var table = $('.representative-list').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('get.representative.data') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'phone', name: 'phone'},
                    {data: 'zone', name: 'zone'},
                    {data: 'action',name: 'action',orderable: false,searchable: false},
                ]
            });

        });
    </script>
    <script>
        function editRepresentative(id) {
            $.get('representative/'+id,function(representative) {
                $("#id").val(representative.id);
                $("#name").val(representative.name);
                $("#phone").val(representative.phone);
                $("#zone").val(representative.zone);
                $("#representativeModalEdit").modal('toggle');
            });
        }

        $("#update_representative").submit(function(e){

            e.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            let id = $("#id").val();
            let name = $("#name").val();
            let phone = $("#phone").val();
            let zone = $("#zone").val();
            let _token = $("input[name= _token]").val();
            $.ajax({
                url :"{{route('update.representative')}}",
                type:"PUT",
                data:{
                    id:id,
                    name:name,
                    phone:phone,
                    zone:zone,
                    _token:_token,
                },
                success:function (response) {
                    if(response){
                        toastr.success(response.message);
                        $("#representativeModalEdit").modal('toggle');
                        $("#update_representative")[0].reset();
                        $('.representative-list').DataTable().ajax.reload();

                    }

                },
                error: function(err){
                    toastr.error("Something went wrong !");
                    $("#representativeModalEdit").modal('toggle');
                    $("#update_representative")[0].reset();
                }
            });
        });

        function deleteRepresentative(id){
            if(confirm('Do you realy want to delete this record?')){
                $.ajax({
                    url:'delete-representative/'+id,
                    type:'get',
                    data:{
                        _token:$("input[name=_token]").val()
                    },
                    success:function(response){
                        toastr.success(response.message);
                        $('.representative-list').DataTable().ajax.reload();
                    },
                    error:function (err){
                        toastr.error("Data can't be deleted !");

                    }
                });
            }
        }
    </script>

@endsection
