<?php

use App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes([
       'register' => false,
       'reset' => true,
       'verify' => true,
]);

Route::get('/home', [Controllers\HomeController::class, 'index'])->name('home');

Route::get('/customers',[Controllers\CustomerController::class,'index'])->name('customers');
Route::get('/customer-list',[Controllers\CustomerController::class,'getAllCustomer'])->name('customer.list');
Route::get('/customer-create',[Controllers\CustomerController::class,'create_customer']);
Route::post('/save-customer',[Controllers\CustomerController::class,'store'])->name('save.customer');
Route::get('/customer/{id}',[Controllers\CustomerController::class,'get_customer_by_id']);
Route::put('/update-customer',[Controllers\CustomerController::class,'update_customer'])->name('update.customer');
Route::get('/delete-customer/{id}',[Controllers\CustomerController::class,'delete_customer'])->name('delete.customer');

Route::get('/representative-list',[Controllers\RepresentativeController::class,'index']);
Route::get('/get-representative_data',[Controllers\RepresentativeController::class,'get_representative_data'])->name('get.representative.data');
Route::get('/create-representative',[Controllers\RepresentativeController::class,'create_representative']);
Route::post('/store-representative',[Controllers\RepresentativeController::class,'store_representative_data'])->name('store.representative.data');
Route::get('representative/{id}',[Controllers\RepresentativeController::class,'get_representative_by_id']);
Route::put('/update-representative',[Controllers\RepresentativeController::class,'update_representative'])->name('update.representative');
Route::get('delete-representative/{id}',[Controllers\RepresentativeController::class,'delete_representative'])->name('delete.representative');


Route::get('/medicinelist',[Controllers\MedicineController::class,'index']);
Route::get('/get-medicine',[Controllers\MedicineController::class,'get_medicine_list'])->name('get.medicine.list');
Route::get('/new-medicine',[Controllers\MedicineController::class,'new_medicine']);
Route::post('/save-medicine',[Controllers\MedicineController::class,'store'])->name('save.medicine');
Route::get('/medicine/{id}',[Controllers\MedicineController::class,'get_medicine_by_id']);
Route::put('update-medicine',[Controllers\MedicineController::class,'update_medicine'])->name('update.medicine');
Route::get('/delete-medicine/{id}',[Controllers\MedicineController::class,'delete_medicine']);

Route::get('/create-invoice',[Controllers\InvoiceController::class,'index']);
Route::post('/invoice-data',[Controllers\InvoiceController::class,'save_invoice_data'])->name('invoice.data');
Route::get('/medicine-list',[Controllers\InvoiceController::class,'medicine_list'])->name('medicine.list');
Route::get('/invoice-list',[Controllers\InvoiceController::class,'invoice_list']);
Route::get('/get-invoice-list',[Controllers\InvoiceController::class,'get_invoice_list'])->name('get.invoice.list');
Route::get('/invoice-details/{id}','App\Http\Controllers\InvoiceController@invoice_view');
Route::get('/invoice-pdf/{id}',[Controllers\InvoiceController::class,'invoice_pdf_view'])->name('pdf.view');
Route::get('/invoice-print/{id}',[Controllers\InvoiceController::class,'get_invoice_print'])->name('print.view');
Route::get('/update-invoice/{id}','App\Http\Controllers\InvoiceController@invoice_update_view');
Route::post('/update-invoice-data/{id}',[Controllers\InvoiceController::class,'save_update_data'])->name('update.invoice.data');


/*Route::get('/customerreport',[Controllers\CustomerReportController::class,'index']);
Route::get('/get_data/{id}/{start_date}/{end_date}',[Controllers\CustomerReportController::class,'get_data']);*/

Route::get('/due-report',[Controllers\DueReportController::class,'index']);
Route::post('/due-report',[Controllers\DueReportController::class,'get_due_report'])->name('due.report');

Route::get('/summary-report',[Controllers\SummaryReportController::class,'index']);
Route::post('/summary-report',[Controllers\SummaryReportController::class,'get_summary_report'])->name('summary.report');


Route::get('/customer-report',[Controllers\CustomerReportController::class,'customer_report']);
Route::post('/get-customer-report',[Controllers\CustomerReportController::class,'get_customer_report']);

Route::get('/representative-report',[Controllers\RepresentativeReportController::class,'index']);
Route::post('/get-representative-report',[Controllers\RepresentativeReportController::class,'get_representative_report']);
